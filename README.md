# Pony-booru-spoiler-blur
A Tampermonkey script which changes the way derpibooru, ponerpics and twibooru spoiler images. Instead of hiding images it blurs them and deblurs on hover.
