// ==UserScript==
// @name         Pony booru spoiler blur
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  Blurs spoilered images, deblurs on hover
// @author       Adrian Kryński
// @source       https://github.com/
// @match        https://derpibooru.org/*
// @match        https://ponerpics.org/*
// @match        https://twibooru.org/*
// @grant        GM_addStyle
// @sandbox      JavaScript
// ==/UserScript==

const hideTags = false;

(function() {
    'use strict';

    GM_addStyle(
      `.blur a {
        filter: blur(10px);
      }
      .blur:hover a {
        filter: none;
      }`
    );

    Array.from(document.querySelectorAll('.js-spoiler-info-overlay:not(:empty)')).forEach(spl => {
        if (spl.textContent == "WebM") return;
        let parent = spl.parentElement;
        let img = parent.querySelector('img');
        let link = JSON.parse(parent.dataset.uris)[parent.dataset.size];
        if ([".mp4", "webm"].includes(link.slice(-4))) link = link.replace(/\.[^/.]+$/, ".gif");

        parent.classList.add('blur');

        if(hideTags) spl.classList.add('hidden');
        img.src = link;

        parent.onmouseleave = () => {
            if(hideTags) spl.classList.add('hidden');
            img.src = link;
        };
    });
})();
